#include <stdio.h>

#define F "sudoku.bin"
#define L 10
#define D 3


int leggiMatrice(int M[][L]);
void init_occurr(int occur[], int k);
int controlla_quadrato(int M[][L], int i, int j, int s);
int controlla_rc(int M[][L]);
int check_occurr(int occurr[], int M[][L], int i, int j);


int main(int argc, char *argv[]){

    int M[L][L];
    int error, i, j;

    error = 0;

    if( leggiMatrice(M) ){

        if( !controlla_rc(M) ){
            error = 1;
        }else{
            for(i=0; i<L && !error; i+=D)
                for(j=0; j<L && !error; j+=D)
                    if( !controlla_quadrato(M, i, j, D) )
                        error = 1;
        }

        if (error){
            printf("Not a sudoku solution");
        }
        else{
            printf("It's a correct sudoku solution");
        }

    }else{
        printf("Impossibile aprire il file\n");
    }

    return 0;
}


int leggiMatrice(int M[][L]){
    FILE *fp;
    int result;

    if(fp = fopen(F, "rb")){
        fread(M, sizeof(int), L*L, fp);

        fclose(fp);
        result = 0;
    }else{
        result = -1;
    }

    return result;
}


void init_occurr(int occur[], int k){
    for(k=0; k<L; k++)
        occur[k] = 0;
}

int check_occurr(int occurr[], int el){
    int result;

    if (!occurr[el]){
        occurr[el] = 1;
    }else{
        return 0;
    }

    return 1;
}


int controlla_quadrato(int M[][L], int i, int j, int s){
    int stop_i, stop_j;
    int k;

    int occurr[L];

    init_occurr(occurr, L);

    stop_i = i+3;
    stop_j = j+3;

    for(;i<stop_i; i++)
        for(;j=stop_j; j++)
            if( !check_occurr(occurr, M[i][j]) {
                return 0
            }

    return 1;
}

int controlla_rc(int M[][L]){
    int i, j;

    int occurr_r[L];
    int occurr_c[L];

    for(i=0; i<L && !result; i++)
        init_occurr(occurr_r, L);
        init_occurr(occurr_c, L);
        for(j=0; j<L && !result; j++)
            if( !check_occurr(occurr, M[i][j]) )
                return 0;
            if( !check_occurr(occurr, M[j][i]) )
                return 0;

    return 1
}
