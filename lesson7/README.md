#lesson 6 - 10/12/2015

## ex. 1 - done
Sviluppare un programma che calcola le aree di tre tipi di figure geometriche: rettangoli, triangoli e quadrati. Il programma dovra' richiedere in input all'utente il tipo di figura e stampare l'area.

Se l'utente inserisce un valore errato per la figura, uscire senza fare alcuna operazione.

## ex. 2 done
Scrivere un programma che acquisica da un file *binario* la temperatura media per i diversi mesi dell’anno in gradi Celsius.
Per ogni mese stampa la temperatura in gradi Fahrenheit e alla fine stampa la media delle temperature osservate.

Assumere che il file di testo contenga le temperature medie come float, in ordine per ogni mese.

conversione:

```
f = ((c * 9 )/5) + 32;
```



## ex. 3 - done (lasciato a loro la funzione per le sottomatrici 3x3)
Scrivere un programma che legga in input da un file di tipo binario una matrice di 9x9 elementi.
Il programma dovra' stampare il contenuto della matrice e verificare che la stessa sia una possibile soluzione
del gioco del sudoku:

```
Il sudoku (giapponese: 数独, sūdoku, nome completo 数字は独身に限る Sūji wa dokushin ni kagiru, che in italiano vuol dire "sono consentiti solo numeri solitari") è un gioco di logica nel quale al giocatore o solutore viene proposta una griglia di 9×9 celle, ciascuna delle quali può contenere un numero da 1 a 9, oppure essere vuota; la griglia è suddivisa in 9 righe orizzontali, 9 colonne verticali e, da bordi in neretto, in 9 "sottogriglie", chiamate regioni, di 3×3 celle contigue. Le griglie proposte al giocatore hanno da 20 a 35 celle contenenti un numero. Lo scopo del gioco è quello di riempire le caselle bianche con numeri da 1 a 9, in modo tale che in ogni riga, colonna e regione siano presenti tutte le cifre da 1 a 9 e, pertanto, senza ripetizioni. In tal senso lo schema, una volta riempito correttamente, appare come un quadrato latino.
```

