#include <stdio.h>

#define MISURAZIONI 12
#define L 30
 
float celsToFahr(float cels){
        return ((cels * 9 )/5) + 32;
}

int acq(float a[]);
int conv(float a[], float b[]);
float media(float a[]);

 
int main(int argc,char* argv[]){
        char file[L+1];
        float temp_c[MISURAZIONI];
        float temp_f[MISURAZIONI];

        int i;
        float avg;
        FILE* fp;
 
        scanf("%s", file);
 
        if ( ( fp = fopen( file, "rb"  ) ) ){

                fread(temp_c, sizeof(float), MISURAZIONI, fp);

                avg = 0;

                for(i=0; i < MISURAZIONI; i++){
                        temp_f[i] = celsToFahr( temp_c[i] );
                        avg += temp_f[i];
                }

                avg = avg/i;
                printf("Temperatura media: %f\n", avg);

                fclose(fp);
        } else {
                fprintf(stderr,"ERRORE: Impossibile aprire il file: %s\n", file);
        }
 
        return 0;
}