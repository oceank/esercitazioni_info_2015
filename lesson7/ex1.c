#include <stdio.h>

#define TRIANGOLO 1
#define QUADRATO 2
#define RETTANGOLO 3
#define NON_VALIDA -1

typedef struct {
    int tipo;
    int a;
    int b;
} figura;


figura leggiFigura();
int calcolaArea(figura f);


int main(int argc, char *argv[]){
    figura f;
    int a;

    f = leggiFigura();

    if (f.tipo != NON_VALIDA){
        a = calcolaArea(f);
        printf("L'area della figura e` %d\n", a);
    }

    return 0;
}


figura leggiFigura(){
    figura f;
    printf("Inserisci il tipo della figura: ");
    scanf("%d", &f.tipo);
    printf("\n");

    if (f.tipo == RETTANGOLO){
        printf("inserisci i valori dei due lati: ");
        scanf("%d %d", &f.a, &f.b);
    }else if (f.tipo == QUADRATO) {
        printf("inserisci il valore del lato: ");
        scanf("%d", &f.a);
        f.b = f.a;
    }else if (f.tipo == TRIANGOLO ){
        printf("inserisci i valori di base e altezza: ");
        scanf("%d %d", &f.a, &f.b);
    }else{
        printf("Figura non valida!");
        f.tipo = -1;
    }

    return f;
}


void leggiFigura_alternativa(figura *f){
    printf("Inserisci il tipo della figura: ");
    scanf("%d", &(f->tipo));
    printf("\n");

    if (f->tipo == RETTANGOLO){
        printf("inserisci i valori dei due lati: ");
        scanf("%d %d", &(f->a), &(f->b));
    }else if (f->tipo == QUADRATO) {
        printf("inserisci il valore del lato: ");
        scanf("%d", &(f->a));
        f->b = f->a;
    }else if (f->tipo == TRIANGOLO ){
        printf("inserisci i valori di base e altezza: ");
        scanf("%d %d", &(f->a), &(f->b));
    }else{
        printf("Figura non valida!");
        f->tipo = -1;
    }

    return;
}


int calcolaArea(figura f)
{
    int area;

    area = f.a * f.b;
    if (f.tipo == TRIANGOLO)
        area /= 2;

    return area;
}
