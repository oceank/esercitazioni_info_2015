/* File: fattoriale.c */
/* Funzione ricorsiva che calcola il fattoriale di un numero >= 0
   Si ricorda che il fattoriale n! e' definito come:

   n! = 1         se n = 0
   n! = n*(n-1)!  se n > 0

*/
/* Esempio di funzione ricorsiva */

#include <stdio.h>

int fattoriale_loop(int n){
  int f;
  int i;

  f =1;
  for(i=n; i>1; i--)
    f=f*i;

  return f;
}

int fattoriale(int n){
  
  if (n < 0) return -1; /* Fattoriale non e' definito per interi negativi! */

  if (n == 0) return 1;
  else return n*fattoriale(n-1);
}

int main(int argc, char *argv[])
{
  int n;
  printf("Inserire un intero >= 0: ");
  scanf("%d", &n);
  printf("Il fattoriale di %d e' %d\n", n, fattoriale(n));

  return 0;
}

/*
  Nota: il fattoriale di un numero cresce molto velocemente.
  Per calcolare il fattoriale di numeri grandi,
  modificare la funzione sostituendo int con long
*/
