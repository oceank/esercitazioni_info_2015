#include <stdio.h>

#define SIZE 3

int minimo(int A[], int i){
    int min;
    if (i == 0)
        return A[i];
    min = minimo(A,i-1);
    if (A[i]<min)
        return A[i];
    else
        return min;
}

int minimo_it(int A[], int n){
    int i, min;

    min=A[0];
    for (i=1; i<n; i++)
        if(A[i] < min)
            min = A[i];
    return min;
}


// test main - non e' da mostrare
int main(int argc, char *argv[]){

    int A[SIZE], i;

    for (i=0; i< SIZE; i++){
        scanf("%d", &A[i]);
    }

    printf("%d %d",
        minimo(A, SIZE-1),
        minimo_it(A, SIZE));

    return 0;
}
