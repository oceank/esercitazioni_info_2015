## ex. 1
Si scriva un sottoprogramma che ricevute in ingresso due liste per la gestione di numeri interi ne crei e restituisca una nuova costituita dagli elementi delle liste di partenza presi in alternanza dall'una e dall’altra lista. Le liste iniziali devono rimanere invariate. Se le liste sono di lunghezza differente non vengono stampati gli elementi che eccedono la lunghezza di una delle sue liste (i.e. prendere il minimo della lunghezza delle due liste o uscire appena si incontra la fine di una delle due liste).

## ex. 2 // skip
scrivere un sottoprogramma ricorsivo che calcoli la somma degli elementi di un array A[1..n] di interi

## ex. 3
Scrivere un sottoprogramma che calcoli il fattoriale per un numero (intero) dato.

Scrivere un programma che calcoli e stampi il fattoriale dei numeri da 1 a 20.

## ex. 4
Scrivere un sottoprogramma che calcoli il Massimo Comun Divisore di due numeri.

## ex. 5
Scrivere un sottoprogramma che calcoli il minimo tra gli elementi di un array formato da massimo n elementi.

## ex. 6
Scrivere il sottoprogramma visto nella esercitazione precedente ```nodo_t * trovaElemento(nodo_t *lista, int num)``` in forma ricorsiva.

## ex. 7 // skip
Scrivere un sottoprogramma per calcolare la lunghezza di una lista in forma ricorsiva

## ex. 8
Scrivere un sottoprogramma per sommare tutti gli elementi (interi) contenuti in una lista

Scrivere un programma che richieda agli utenti una lista di interi, terminata da 0, faccia la somma di tutti gli interi e quindi stampi il risultato dell'operazione nel seguente formato ```5+1+3+6+7+8+9= <somma>```
(l'ordine degli elementi della somma non conta, utilizzare l'inserimento in testa)
