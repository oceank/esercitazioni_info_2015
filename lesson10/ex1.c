nodo_t *alternaLista (nodo_t *lista1, nodo_t *lista2){
        nodo_t *nuova;
 
        for (nuova=NULL; lista1 && lista2;) {
                if (lista1) {
                    nuova = inserisciInCoda(nuova, lista1->num);
                    lista1=lista1->next;
                }
                if (lista2) {
                    nuova = inserisciInCoda(nuova, lista2->num);
                    lista2=lista2->next;
                }
        }
 
        return nuova;
}