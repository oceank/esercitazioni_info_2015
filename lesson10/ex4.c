#include <stdio.h>

/* algoritmo di Euclide per sottrazioni successive
"Se due numeri, m, n, sono divisibili per un terzo numero, x,
allora anche la loro differenza è divisibile per x"

Per dimostrarla, si può utilizzare la proprietà distributiva.
Supponiamo m>n.
m=kx
n=hx
m-n=kx-hx=x(k-h)
Dunque si può dire che:
MCD(m,n) = MCD((m-n),n)

Come si vede, questa regola permette di passare, per mezzo
di sottrazioni successive, a MCD di numeri sempre più piccoli,
fino ad ottenere:
MCD(a,0)=a

L'algoritmo può essere scritto così:
Finché m>0
se n>m allora scambia m con n
Sottrai n da m e assegna ad m il valore della differenza (m=m-n)
Ripeti il ciclo n è il MCD cercato
*/

/* codifica ricorsiva */
int  MCDric(int a, int b)
{
  int differenza, minore;
    if(a == b)
      return (b);
    else {
        if (a>b){
            differenza = a-b;
            minore = b;
        }else{
            differenza = b-a;
            minore = a;
        }
        return (MCDric(differenza,minore));
    }
}

/* codifica iterativa */
int  MCDite(int a, int b)
{
  while (a != b) {
    if (a > b)
        a = a - b;
    else
        b = b - a;
  }
  return (b);
}

int main (int argc, char *argv[])
{
  int x, y;
  printf("introdurre due numeri interi\n");
  scanf("%d %d", &x, &y);
  printf("Il Massimo Comun Divisore fra %d e %d e' %d\n",
    x, y, MCDric(x,y));
  printf("Il Massimo Comun Divisore fra %d e %d e' %d\n",
    x, y, MCDite(x,y));

  return 0;
}
