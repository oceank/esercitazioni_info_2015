typedef struct nodo {
  int num;
  struct nodo *next;
} nodo_t;


int length (nodo_t *l)
{
    if (l == NULL)
      return 0;
    else
      return 1 + length(l->next);
}

// questo conviene piu' o meno in termini di memoria?
// -> passaggio per copia
int length2 (nodo_t l)
{
    if (l == NULL)
      return 0;
    else
      return 1 + length(l.next);
}
