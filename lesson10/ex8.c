#include <stdio.h>
#include <stdlib.h>

#define SENT 0

typedef struct nodo {
  int info;
  struct nodo *next;
} nodo_t;


nodo_t * creaNodo(int v){
    nodo_t *nodo;
    nodo = (nodo_t*) malloc (sizeof (nodo_t));
 

 
    return nodo;
}


nodo_t * inserisciInTesta(nodo_t *lista, int v){
    nodo_t *nodo;
    nodo=creaNodo( v );
    if (nodo){
        nodo->next = lista;
        return nodo;
    } else {
        return lista;
    }
}


nodo_t * inserisciInTesta2(nodo_t *lista, int v){
    nodo_t *nodo;
    nodo = (nodo_t*) malloc (sizeof (nodo_t));

    if ( nodo ){
        nodo->info = v;
        nodo->next = lista;
        lista = nodo;
    } else {
        printf("Errore allocazione memoria per %d", v);
    }
    
    return lista;
}


void liberaMemoria(nodo_t *h){
    nodo_t *cur;
    while(h!=NULL){
        cur = h;
        h = h->next;
        free(cur);
    }
}

int sum(nodo_t *h){
    
    if (h==NULL){
        return 0;
    } else {
        return h->info+sum(h->next);
    }
}

void stampaSomma(nodo_t *h, int n){

    char c;

    if(h == NULL)
        return;

    stampaSomma(h->next, n+1);

    if(n==0){
        c = '=';
    }else{
        c = '+';
    }

    printf("%d%c", h->info, c);
}

int main(int argc, char *argv[]){
    int n, s;
    nodo_t *h;

    h = NULL;

    // acquisizione
    scanf("%d", &n);
    while(n!=SENT){
        h = inserisciInTesta(h, n);
        scanf("%d", &n);
    }

    // somma
    s = sum(h);

    stampaSomma(h, 0);
    printf("%d\n", s);

    liberaMemoria(h);
    return 0;
}