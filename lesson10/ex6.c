typedef struct nodo {
  int num;
  struct nodo *next;
} nodo_t;


nodo_t * trovaElemento(nodo_t *lista, int num){
    if (lista == NULL)
        return NULL;

    if (lista->info == num )
        return lista;

    return trovaElemento(lista->next, num);
}
