#include <stdio.h>

#define MAX_GRADE 10+1
#define SUMS 2
// 10+1 consider also grade 0

typedef struct polinomio_s {
    int coeffs[MAX_GRADE];
} t_polinomio;

int main( int argc, char *argv[]){
    int i, s;
    t_polinomio p[SUMS];
    t_polinomio p_somma;
    int need_sum;


    for( i = 0; i < MAX_GRADE; i++ ){
        p_somma.coeffs[i] = 0;
    }

    for( s = 0; s < SUMS; s++ ){
        printf("inserisci un polinomio:\n");

        for( i = MAX_GRADE-1; i >= 0; i-- ){
            printf("coeff x^%d: ", i);
            scanf("%d", &(p[s].coeffs[i]));
        }
        printf("\n");
    }

    /* somma */
    for ( s = 0; s < SUMS; s++ ){
        for ( i = 0; i < MAX_GRADE; i++ ){
            p_somma.coeffs[i] = p_somma.coeffs[i] + p[s].coeffs[i];
        }
    }

    for ( need_sum = 0, i = MAX_GRADE-1; i > 0; i-- ){

        if ( need_sum ){
            printf(" + ");
            need_sum = 0;
        }

        if ( p_somma.coeffs[i] !=0 ){
            printf( "%dx^%d", p_somma.coeffs[i], i);
            need_sum = 1;
        }

    }


    if ( p_somma.coeffs[i] !=0 ){
        if ( need_sum )
            printf(" + ");

        printf( "%d", p_somma.coeffs[i]);
    }

    return 0;
}
