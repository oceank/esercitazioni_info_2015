#include <stdio.h>

#define MAXV 20
#define MAXD 200

#define NVOCI 100


typedef struct voce_s {
    char voce[MAXV+1];
    char descrizione[MAXD+1];
} voce_t;



int main(int argc, char *argv[]){
    voce_t glossario[NVOCI];
    char scelta;
    int i;

    for ( i = 0; i < NVOCI ; i++ ){
        printf("Inserisci la voce %d:", i+1)
        gets(glossario[i].voce);

        printf("Inserisci la descrizione: ");
        gets(glossario[i].descrizione);
    }
    printf("\ninserisci il carattere da cercare: ")
    scanf("%c", &scelta);

    for ( i = 0; i < NVOCI; i++)
        if ( glossario[i].voce[0] == scelta )
            printf("%s: %s", glossario[i].voce, glossario[i].descrizione);


    return 0;
}
