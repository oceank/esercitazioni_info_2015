#lesson 5 - /11/2015

## ex. 1
Sviluppare un programma che svolge le seguenti operazioni:

  + Acquisisce informazioni relative ad una serie di rilievi altimetrici, **fino ad un massimo di 10 rilievi**.
      (i.e. richiedere all'utente se vuole inserire un dato o meno)
      Ogni rilievo è caratterizzato da una latitudine, una longitudine ed una altitudine.
  + Terminata la fase di acquisizione, stampa sullo schermo le informazioni relative a tutti i rilievi per
      i quali il valore della longitudine è pari


## ex. 2
Si sviluppi un programma che svolge le seguenti operazioni:

  + Acquisisce le informazioni relative a cinque studenti e le memorizza in un array.
  + Richiede all’utente di inserire il numero di matricola di uno studente di cui cercare le informazioni
      nell’array.
  + Effettua la ricerca e stampa i dati che eventualmente verranno trovati nell’array.


## ex. 3
Un polinomio è la somma di più termini, ciascuno dei quali è costituito da un coefficiente (intero per semplicità)
e di una potenza di x (non negativa, per semplicità). Ad esempio x^2 + 1.
Non necessariamente compaiono tutte le potenze di x. Si ipotizzi, che il massimo grado sia 10 (x^10).
Definire un tipo di dato adatto a rappresentare i termini dei polinomi.
Scrivere un programma che acquisisce i dati di due polinomi e calcola (deve creare un nuovo polinomio)
e visualizza il polinomio somma.

## ex. 4
Definire un tipo di dato per la gestione di un glossario (composto da voci di al più 20 caratteri e dalla loro descrizione
di al più 200 caratteri). Scrivere un programma che
  + chiede all’utente di inserire 100 voci e la loro descrizione,
  + chiede all’utente di inserire un carattere
  + cerca nelle voci inserite quelle che iniziano con il carattere ricevuto dall’utente,
      visualizza le voci trovate e la loro descrizione, e il numero di voci trovate.

voci e descrizioni sono tutte inserite in caratteri minuscoli.

## ex. 5
Acquisire una matrice di interi di dimensione *massima* 10x10 e calcolare la trasposta.

  +  richiedere all'utente la dimensione della matrice
  +  verificare che l'utente abbia inserito una dimensione valida
  +  assumere che non ci siano errori nell'inserimento della matrice

