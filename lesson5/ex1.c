#include <stdio.h>

#define MAX_RIL 10

typedef struct rilievo_s {
    int longit;
    int latid;
    int altid;
} rilievo;

typedef struct archivio_s {
    rilievo a[MAX_RIL];
    int numRilievi;
}archivio;

int main(int argc, char *argv[])
{
    archivio arch;
    int decisione, i;
    arch.numRilievi=0;
    /* acquisizione dati */

    printf("Vuoi inserire un dato (se si digita 1, altrimenti 0): ");
    scanf("%d", &decisione);

    while (decisione && arch.numRilievi<MAX_RIL)
    {
        scanf("%d", &(arch.a[arch.numRilievi].longit));
        scanf("%d", &(arch.a[arch.numRilievi].latid));
        scanf("%d", &(arch.a[arch.numRilievi].altid));
        arch.numRilievi++;
        printf("Vuoi inserire un nuovo dato (se si digita 1, altrimenti 0): ");
        scanf("%d", &decisione);
    }

    /* Individuazione e stampa degli elementi con valore di longitudine
     * pari */
    for(i=0;i<arch.numRilievi;i++)
       if(arch.a[i].longit%2==0)
           printf("\n%d, %d, %d\n", arch.a[i].longit, arch.a[i].latid,
           arch.a[i].altid);
    return 0;
}
