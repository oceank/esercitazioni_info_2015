#include <stdio.h>

#define MAX 10


typedef struct mat_s{
    int M[MAX][MAX];
    int m, n;
} mat_t;


int main(int argc, char *argv[]){

    mat_t A, T;
    int i,j;


    do{
        printf("inserire la dimensione: ");
        scanf("%d %d", &(A.m), &(A.n));
    } while( A.m < 0 || A.m > MAX || A.n < 0 || A.n > MAX);

    printf("inserire la matrice:\n");

    for (i = 0; i < A.m; i++ )
        for (j = 0; j < A.n; j++)
            scanf("%d", &A.M[i][j] );

    printf("\n");

    T.m = A.n;
    T.n = A.m;

    for ( i = 0 ; i < A.m; i++ )
        for (j = 0; j < A.n; j++ )
            T.M[j][i] = A.M[i][j];

    /* le righe sono le colonne ora, teniamone conto per gli indici */
    for ( i = 0; i < T.m; i++ ){
        for ( j = 0; j < T.n; j++ ){
            printf("%d ", T.M[i][j]);
        }
        printf("\n");
    }


    return 0;
}
