#include <stdio.h>
#define MAX_STR 20
#define MAX_NUM_EL 5

typedef struct studente_s {
    int matricola;
    char nome[MAX_STR];
    char cognome[MAX_STR];
} studente;

int main(int argc, char *argv[])
{
    studente st[MAX_NUM_EL];
    int i, j;
    int matr;
    int trovato;

    for (i=0;i<MAX_NUM_EL;i++)
    {
        /*acquisizione dei dati di ogni studente */
        printf("Inserisci le informazioni di uno studente\n");
        printf("nome: ");
        scanf("%s", st[i].nome);
        printf("cognome: ");
        scanf("%s", st[i].cognome);
        printf("matricola: ");
        scanf("%d", &st[i].matricola);
        printf("\nDati acquisiti.\n");
    }

    printf("inserisci la matricola dello studente: ");
    scanf("%d", &matr);
    trovato = 0;

    for (i=0; i<MAX_NUM_EL && !trovato; i++)
        if (matr == st[i].matricola)
            trovato = 1;

    if (trovato)
    {
        printf("informazioni sullo studente: \n");
        printf("matricola: %d\n", st[i-1].matricola);
        printf("nome: %s\n", st[i-1].nome);
        printf("cognome: %s\n", st[i-1].cognome);
    }
    else printf("informazioni non presenti in archivio\n");

    return 0;
}


