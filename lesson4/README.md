#ex. 1
Scrivere un programma che acquisisce una stringa di al più 500 caratteri e stampa il numero di parole nella stringa.
I caratteri che sono da considerare separatori di parole sono ‘ ‘, ‘\t’, ‘\n’, ‘\r’

Ad esempio la stringa Ciao come stai? contiene 3 parole


#ex. 2
Si implementi un programma che acquisisce i tempi di percorrenza di un giro di un gran premio di formula 1,
effettuati dai 22 piloti. Il programma si aspetta cognome pilota (senza eventuali spazi) seguito dal tempo
(misurato in secondi, con un numero reale). Il programma individua i migliori 5,
quindi stampa l’elenco dei piloti nello stesso ordine ricevuto dall’utente.
I migliori 5 piloti vengono marchiati con un etichetta “BEST 1”, “BEST 2”, “BEST 3”, “BEST 4”, “BEST 5”.

Tempi in input: Alonso 88.709 Bianchi 90.171 Bottas 88.928 Button 88.814 Chilton 90.335 DiResta 89.300 Grosjean
88.796 Gutierrez 88.682 Hamilton 89.052 Hulkenberg 88.947 Maldonado 89.012 Massa 88.886 Perez 88.503 Pic 92.907
Raikkonen 87.679 Ricciardo 88.831 Rosberg 88.816 Sutil 88.419 VanDerGarde 162.390 Vergne 89.280 Vettel 88.116 Webber 89.500

Output programma: Alonso 88.709000 Bianchi 90.170998 Bottas 88.928001 Button 88.814003 Chilton 90.334999 DiResta
89.300003 Grosjean 88.795998 Gutierrez 88.681999 BEST 5 Hamilton 89.052002 Hulkenberg 88.946999 Maldonado 89.012001
Massa 88.886002 Perez 88.502998 BEST 4 Pic 92.906998 Raikkonen 87.679001
BEST 1 Ricciardo 88.831001 Rosberg 88.816002 Sutil 88.418999
BEST 3 VanDerGarde 162.389999 Vergne 89.279999 Vettel 88.115997
BEST 2 Webber 89.500000

#ex. 3
Si scriva un programma che calcoli lo stipendio medio dei dipendenti di una azienda con un dato nome.
Nell'azienda ogni dipendente occupa un ufficio rappresentato da una sigla di tre caratteri.

Il programma dovra':

  + acquisire il nome del dipendente i.e. "Giacomo" da tastiera
  + acquisire i dati degli uffici da tastiera (4 uffici)
  + Stampare lo stipendio medio degli impiegati dell'azienda che si chiamano con il nome dato
      i.e. ``Giacomo``.
lo stipendio e' un numero reale
il nome del dipendente e' di massimo 50 caratteri

#ex. 4
Scrivere un programma in linguaggio C che legga una frase introdotta da tastiera. La
frase è terminata dall’introduzione del carattere di invio. La frase contiene sia caratteri
maiuscoli che caratteri minuscoli, e complessivamente al più 100 caratteri. Il programma
deve svolgere le seguenti operazioni:

  + visualizzare la frase inserita
  + costruire una nuova frase in cui il primo carattere di ciascuna parola nella frase di

partenza è stato reso maiuscolo. Tutti gli altri caratteri devono essere resi minuscoli.
Il programma deve memorizzare la nuova frase in una opportuna variabile

  + visualizzare la nuova frase.

Ad esempio la frase ``cHe bElLA gIOrnaTa`` diviene ``Che Bella Giornata``.

#ex. 5
Acquisire da tastiera base e altezza (numeri reali) di un rettangolo e memorizzarle in una struct, calcolare:

  + area
  + perimetro

#ex. 6
Un polinomio è la somma di più termini, ciascuno dei quali è costituito da un coefficiente (intero per semplicità) e di una potenza di x (non negativa, per semplicità).
Ad esempio x^2 + 1. Non necessariamente compaiono tutte le potenze di x. Si ipotizzi, che il massimo grado sia 10 (x^10).
Definire un tipo di dato adatto a rappresentare i termini dei polinomi. 
Scrivere un programma che acquisisce i dati di due polinomi e calcola (deve creare un nuovo polinomio) e visualizza il polinomio somma

#ex. 7
Definire un tipo di dato per la gestione di una semplice "rubrica" [DNS](https://it.wikipedia.org/wiki/Domain_Name_System)
ad ogni dominio (ad esempio www.wikipedia.org) corrisponde un indirizzo [IPv4](https://it.wikipedia.org/wiki/IPv4).
Un indirizzo IPv4 e' formato da quattro numeri che vanno da 0 a 256 e vengono separati da un punto: ad esempio 127.0.0.1 o 131.175.14.137.

Il programma dovra'
  + richiedere all'utente 20 coppie composte da una stringa che rappresenta il dominio e l'indirizzo e li memorizzi in maniera adatta.
  + dopo la fase di memorizzazione richiedere all'utente una stringa in input e stampare il relativo indirizzo IP

## cose in piu'
  + richiedere l'indirizzo tramite un ciclo fino a che l'input non corrisponde a "exit" in tal caso terminare il programma
  + salvare l'indirizzo in un dato di tipo int.

