## ex. 1
Scrivere un sottoprogramma per inserire in una lista ordinata.

Scrivere un programma che richieda all'utente una lista di numeri interi terminata da 0 (escludere quindi questo numero dalla lista)
e la stampi in maniera ordinata.

## ex. 2
Si supponga che i sottoprogrammi ```inserisciInCoda(nodo_t *, int)``` e ```inserisciInTesta(nodo_t *, int)``` siano dati.
Si definisca un sottoprogramma, copiaSenzaDuplicati, che riceve in ingresso una lista e restituisce il puntatore ad una nuova lista che non contiene valori duplicati.
Scrivere un programma che legge da console una sequenza di interi, li memorizza nella lista opportuna,
ed utilizza il sottoprogramma copiaSenzaDuplicati per creare e poi visualizzare la stessa sequenza di interi senza duplicati.

## ex. 3
Scrivere un programma che legge da console una sequenza di interi terminate dallo zero e la salva in una lista. Il programma inoltre chiede due numeri all’utente, e sostituisce tutte le occorrenze del primo con il secondo e stampa la lista risultante.
