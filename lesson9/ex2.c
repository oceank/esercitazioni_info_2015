#include <stdio.h>
#include <stdlib.h>


#define SENT 0
 
typedef struct nodo {
  int num;
  struct nodo *next;
} nodo_t;
 
 
nodo_t * trovaElemento(nodo_t *lista, int num){
    while ( lista != NULL && lista->num != num ){
        lista = lista->next;
    }
    /* Alternativa
    for(;lista != null && lista->num != num; lista=lista->next );
    */
 
    return lista;
}
 
nodo_t * creaNodo(int v){
    nodo_t *nodo;
    nodo = (nodo_t*) malloc (sizeof (nodo_t));
 
    if ( nodo )
        nodo->num = v;
    else
        fprintf(stderr,"Errore allocazione memoria per %d", v);
 
    return nodo;
}
 
nodo_t * inserisciInCoda(nodo_t *lista, int v){
    nodo_t *nodo;
    if ( lista == NULL ) {
        lista=creaNodo( v );
    } else {
        for( nodo=lista; nodo->next != NULL; nodo=nodo->next );
        nodo->next=creaNodo( v );
    }
 
    return lista;
}
 
 
void stampaLista( nodo_t *lista ){
    for( ; lista != NULL; lista = lista->next ){
        printf("%d ",lista->num);
    }   
}
 
 
nodo_t * copiaSenzaDuplicati(nodo_t *lista){
    nodo_t *nuova;
 
    nuova = NULL;
    while(lista){
        if ( ! trovaElemento( nuova, lista->num ) ){
            nuova = inserisciInCoda( nuova, lista->num );
        }       
 
        lista=lista->next;
    }   
 
    return nuova;
}

void liberaMemoria(nodo_t *h){
    nodo_t *cur;
    cur=h;
    while(h!=NULL){
        cur = h;
        h = h->next;
        free(cur);
    }
}

int main(int argc, char* argv[]){
    int n;
    nodo_t *lista;
    nodo_t *listaNoDup;
 
    lista = NULL;
    scanf("%d",&n);
    while( n != SENT ){
        lista = inserisciInCoda( lista, n );
        scanf("%d",&n);
    }   
 
    stampaLista( lista );
 
    printf("\n");
    listaNoDup = copiaSenzaDuplicati( lista );
    stampaLista( listaNoDup );
 
    liberaMemoria( lista );
    liberaMemoria( listaNoDup );
    lista = NULL;
    listaNoDup = NULL;

    return 0;
}