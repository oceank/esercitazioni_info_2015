#include <stdio.h>
#include <stdlib.h>

typedef struct _el {
  int info;
  struct _el * next;
} elem_t;

void ins_testa(elem_t ** h, int val)
{
  elem_t * nuovo;
  
  if(nuovo = (elem_t *)malloc(sizeof(elem_t))){
    nuovo->info = val;
    nuovo->next = *h;
    *h = nuovo;
  } else 
    printf("ins_testa: problema di memoria");
  
  return ; 

}

elem_t * ins_coda(elem_t * h, int val)
{
  elem_t * nuovo, * tmp;
  
  if(nuovo = (elem_t *)malloc(sizeof(elem_t))){
    nuovo->info = val;
    nuovo->next = NULL;
    if(!h)
      h = nuovo;
    else{
      for(tmp = h; tmp->next; tmp = tmp->next);
      tmp->next = nuovo;
    }
  } else 
    printf("ins_coda: problema di memoria");
  
  return h; 

}

// Uso di un caso speciale per la testa
void SortedInsert(elem_t** head, elem_t* newEl) {

    elem_t* current;

    // Special case per la testa
    if (*head == NULL || (*head)->info >= newEl->info) {
        newEl->next = *head;
        *head = newEl;
    }
    else {
        // Locate the elem_t before the point of insertion
        current = *head;
        while (current->next!=NULL && current->next->info<newEl->info) {
            current = current->next;
        }
        newEl->next = current->next;
        current->next = newEl;
    }

    return;
}

// Utilizzando un nodo dummy
void SortedInsert2(elem_t** head, elem_t* newEl) {
    elem_t dummy;
    elem_t* current;

    current = &dummy;
    dummy.next = *head;

    while (current->next!=NULL && current->next->info<newEl->info) {
        current = current->next;
    }
    newEl->next = current->next;
    current->next = newEl;
    *head = dummy.next;

    return;
}

// Local references strategy for the head end
void SortedInsert3(elem_t** head, elem_t* newEl) {
    elem_t** currentRef;

    currentRef = head;

    while (*currentRef!=NULL && (*currentRef)->info<newEl->info) {
        currentRef = &((*currentRef)->next);
    }
    newEl->next = *currentRef;

    *currentRef = newEl;

    return;
}

elem_t * del(elem_t * h)
{
  elem_t * el;

  if(h){
    el = h;
    h = h->next;
    free(el);
  }  
  
  return h; 

}

void freeAll(elem_t *h){
    elem_t *cur;

    while(h!=NULL){
        cur = h;
        h = h->next;
        free(cur);
    }

    return;
}

void printAll(elem_t *h){
    elem_t *cur;

    cur = h;

    while(cur!=NULL){
        printf("%d ", cur->info);
        cur = cur->next;
    }    
}

int main(int argc, char *argv[]){

    int c;
    elem_t* el;
    elem_t* h;

    h = NULL;

    scanf("%d", &c);
    while(c!=0){
        el = (elem_t *)malloc(sizeof(elem_t));
        el->info = c;
        SortedInsert(&h, el);
        scanf("%d", &c);
    }

    printAll(h);

    freeAll(h);
    h=NULL;

    return 0;
}
