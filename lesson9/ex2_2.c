#include <stdio.h>
#include <stdlib.h>


#define SENT 0
 
typedef struct nodo {
  int num;
  struct nodo *next;
} nodo_t;
 
 
nodo_t * trovaElemento(nodo_t *lista, int num){
    while ( lista != NULL && lista->num != num ){
        lista = lista->next;
    }
    /* Alternativa
    for(;lista != null && lista->num != num; lista=lista->next );
    */
 
    return lista;
}
 
nodo_t * creaNodo(int v){
    nodo_t *nodo;
    nodo = (nodo_t*) malloc (sizeof (nodo_t));
 
    if ( nodo )
        nodo->num = v;
    else
        printf("Errore allocazione memoria per %d", v);
 
    return nodo;
}

nodo_t * inserisciInTesta(nodo_t *lista, int v){
    nodo_t *nodo;
    nodo=creaNodo( v );
    nodo->next = lista;
    return nodo;
}
 
nodo_t * copiaSenzaDuplicati(nodo_t *lista){
    nodo_t *nuova;

    nuova = NULL;
    while(lista){
        if ( ! trovaElemento( nuova, lista->num ) ){
                nuova = inserisciInTesta( nuova, lista->num );
        }

        lista=lista->next;
    }

    return nuova;
}

void stampaLista( nodo_t *lista ){
    for( ; lista != NULL; lista = lista->next ){
        printf("%d ",lista->num);
    }   
}

void liberaMemoria(nodo_t *h){
    nodo_t *cur;
    cur=h;
    while(h!=NULL){
        cur = h;
        h = h->next;
        free(cur);
    }
}
 
int main(int argc, char* argv[]){
    int n;
    nodo_t *lista;
    nodo_t *listaNoDup;

    lista = NULL;
    scanf("%d",&n);
    while( n != SENT ){
            lista = inserisciInTesta( lista, n );
            scanf("%d",&n);
    }

    stampaLista( lista );
    listaNoDup = copiaSenzaDuplicati( lista );
    stampaLista( listaNoDup );

    liberaMemoria( lista );
    liberaMemoria( listaNoDup );
    lista = NULL;
    listaNoDup = NULL;

    return 0;
}
