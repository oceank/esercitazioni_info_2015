#include <stdio.h>
#include <stdlib.h>

#define SENT 0

typedef struct nodo {
  int num;
  struct nodo *next;
} nodo_t;

nodo_t * creaNodo(int v){
    nodo_t *nodo;
    nodo = (nodo_t*) malloc (sizeof (nodo_t));
 
    if ( nodo )
        nodo->num = v;
    else
        fprintf(stderr,"Errore allocazione memoria per %d", v);
 
    return nodo;
}
 
nodo_t * inserisciInCoda(nodo_t *lista, int v){
    nodo_t *nodo;
    if ( lista == NULL ) {
        lista=creaNodo( v );
    } else {
        for( nodo=lista; nodo->next != NULL; nodo=nodo->next );
        nodo->next=creaNodo( v );
    }
 
    return lista;
}
 
 
void stampaLista( nodo_t *lista ){
    for( ; lista != NULL; lista = lista->next ){
        printf("%d ",lista->num);
    }   
}


nodo_t * trovaElemento(nodo_t *lista, int num){
    while ( lista != NULL && lista->num != num ){
        lista = lista->next;
    }
    /* Alternativa
    for(;lista != null && lista->num != num; lista=lista->next );
    */
 
    return lista;
} 

void sostituisciTutti(nodo_t *lista, int cerca, int sost ){
        for ( ; lista!=NULL; lista=lista->next) {
                if ( cerca == lista->num )
                 lista->num = sost;
        }
}
 
/*Versione che riutilizza la funzione trovaElemento */
void sostituisciTutti2(nodo_t *lista, int cerca, int sost ){
        lista = trovaElemento( lista, cerca );
        while ( lista ) {
                lista->num = sost;
                lista = trovaElemento( lista->next, cerca );
        }
}

void liberaMemoria(nodo_t *h){
    nodo_t *cur;
    cur=h;
    while(h!=NULL){
        cur = h;
        h = h->next;
        free(cur);
    }
}
 
int main(int argc, char* argv[]){
    int n, s;
    nodo_t *lista;
    nodo_t *listaNoDup;

    lista = NULL;
    scanf("%d",&n);
    while( n != SENT ){
            lista = inserisciInCoda( lista, n );
            scanf("%d",&n);
    }

    printf("Numero da sostituire: ");
    scanf("%d",&n);
    printf("Numero con cui sostituirlo: ");
    scanf("%d",&s);


    sostituisciTutti( lista, n, s );

    stampaLista( lista );


    liberaMemoria( lista );
    return 0;        
}