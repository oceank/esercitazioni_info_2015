#lesson 6 - 10/12/2015

## ex. 1
Scrivere un programma che legga una matrice da un file binario (contenente float), e la visualizzi.
Assumere che la matrice sia di dimensione 10x10.

## ex. 3
Scrivere un sottoprogramma che ordini un array di interi fornito in ingresso

## ex. 4
Scrivere un programma che richieda all'utente un array di interi, li ordini in maniera decrescente: e li visualizzi.
L'utente puo' inserire al massimo 10 elementi;
Si ritenga la sequenza terminata quando l'utente inserisce -1;

## ex. 5
Modificare il programma del punto precedente per leggere l'array da un file binario
Assumere che il file contenga sempre MASSIMO 10 elementi;
Richiedere all'utente il nome del file (massimo 20 caratteri);