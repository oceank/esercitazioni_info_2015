#include <stdio.h>

#define N 10
#define S -1
#define LF 20

int leggi(int n[]);
int leggidafile(int n[]);
int ordina(int n[], int l);
int stampa(int n[], int l);


int main(int argc, char *argv[]){

    int n[N];
    int l;

    l = leggi(n);
    ordina(n, l);
    stampa(n, l);

    return 0;
}


int leggi(int n[]){
    int i;

    i=0;
    scanf("%d", &n[i]);
    i++;
    while(i<N && n[i]!=S ){
        scanf("%d", &n[i]);
        i++;
    }

    return i;
}

int leggidafile(int n[]){

    int l;
    FILE *fp;
    char nome[LF+1];

    scanf("%s", nome);

    l = 0;

    if(fp = fopen(nome, "rb")){
        l = fread(n, sizeof(int), N, fp);
        fclose(fp);
    }

    return l;
}

int ordina(int n[], int l){
    int i,j;
    int tmp;

    for(i=0; i<l; i++){
        for(j=i+1; j<l; j++){
            if(n[i]<n[j]){
                tmp = n[i];
                n[i] = n[j];
                n[j] = tmp;
            }
        }
    }
}

int stampa(int n[], int l){
    int i;
    for(i=0; i<l; i++)
        printf("%d ", n[i]);
}
