#include <stdio.h>

#define F "matrix.bin"
#define L 10


int leggiMatrice(float M[][L]);
void visualizzaMatrice(float M[][L]);

int main(int argc, char *argv[]){

    float M[L][L];

    if( leggiMatrice(M) ){
        visualizzaMatrice(M);
    }else{
        printf("cannot read matrix!");
    }

    return 0;
}


int leggiMatrice(float M[][L]){
    FILE *fp;
    int result;

    if(fopen(F, "rb")){
        fread(M, sizeof(float), L*L, fp);

        fclose(fp);
        result = 0;
    }else{
        result = -1;
    }

    return result;
}

void visualizzaMatrice(float M[][L]){
    int i, j;

    for(i=0;i<L;i++){
        for(j=0;j<L;j++){
            printf("%f\n", M[i][j]);
        }
        printf("\n");
    }
}