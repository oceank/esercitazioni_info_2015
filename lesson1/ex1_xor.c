#include <stdio.h>

int main(int argc, char *argv[])
{
    int a,b; // input variables

    printf("insert two numbers a and b\n");
    scanf("%d %d", &a, &b);

    /* exchange the numbers */
    a = a^b;
    b = a^b;
    a = a^b;

    /* print the results */

    printf("a: %d, b: %d\n", a, b) ;
    return 0;
}
