#include <stdio.h>

#define LNOME 40
#define LCOGN 20
#define LP 100

#define F "persone.bin"

typedef struct {
        char nome[LNOME+1];
        char cognome[LCOGN+1];
        int eta;
} persona_t;

int caricaPersone( persona_t p[]);
int caricaPersoneDaFile( persona_t *p );
void stampaPersona( persona_t *p);


int main(int argc, char* argv[]){
        persona_t persone[LP];
        int i, len;

        len = caricaPersone( persone );

        for( i=0; i<len; i++ ){
                stampaPersona( &persone[i] );
        }
 
        return 0;
}


int caricaPersone( persona_t p[]){
    int len;
    char c;

    printf("Inserire dati relativi ad una persona? [Y/N]");
    scanf("%c",&c);
    len=0;
    while( c== 'Y' && len<LP){
            scanf("%s %s %d",p->nome,p->cognome,&p->eta);
            /* Alternativa:
            scanf("%s %s %d",(*p).nome,(*p).cognome,&(*p).eta); */
            printf("Inserire dati relativi ad una persona? [Y/N]");
            scanf("%c",&c);
            scanf("%c",&c);
            len++;
    }
    return len;
}

int caricaPersoneDaFile( persona_t *p ){
    FILE *fp;
    int len;
    char c;

    len=0;

    if(fp = fopen(F, "rb"))
    while( !feof(fp) && len<LP){
            fread(p, sizeof(persona_t), 1, fp);
            len++;
    }
    return len;

}

void stampaPersona( persona_t *p){
        printf("%s %s %d\n",p->nome,p->cognome,p->eta);
}
