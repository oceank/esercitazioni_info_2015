#include <stdio.h>

#define LEN 30
#define SENT 0


void infoArray( int seq[], int len, int *min, int *max, float *med);


int main(int argc, char* argv[]){
        int interi[LEN];
        int min, max;
        int i, n;
        float avg;
 
        i = 0;
        scanf( "%d", &n );
        while ( n != SENT ){
                interi[i] = n;
                i++;
                scanf("%d", &n);
        }
 
        if ( i > 0 ) {
                infoArray( interi, i, &min, &max, &avg );
        } else {
                min = 0;
                max = 0;
                avg = 0;
        }
 
        printf("%d %d %f",min,max,avg);
}


void infoArray( int seq[], int len, int *min, int *max, float *med){
        int i, tot;
 
        tot = 0;
 
        for( i = 0, *min=seq[i], *max=seq[i] ; i < len; i++ ){
                if( seq[i] < *min )
                        *min = seq[i];
                else if ( seq[i] > *max )
                        *max = seq[i];
 
                tot = tot + seq[i];
        }
 
        *med = (float)tot / len;
}