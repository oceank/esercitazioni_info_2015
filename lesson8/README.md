## ex. 1
Scrivere un sottoprogramma, infoArray, che ricevuto un array di interi (si assuma dimensione > 0) e qualsiasi altro parametro ritenuto strettamente necessario, trasferisca al chiamante il valore massimo, il valore minimo e la media.
Scrivere un programma che legga una sequenza di al più 30 interi terminata dallo zero (lo ZERO non fa parte della sequenza), ed invochi il sottoprogramma infoArray per identificare il valore massimo, il valore minimo e la media della sequenza e stamparli.

## ex. 2
Definire un tipo di dato adeguato per memorizzare nome, cognome ed età di una persona. Si supponga che nome e cognome non possano avere più di 40 e 20 caratteri rispettivamente.

Scrivere un sottoprogramma, inserisciPersona, che riceva un dato di tipo persona lo popoli con dei valori inseriti dall’utente a console.   Scrivere un sottoprogramma, stampaPersona, che ricevuto un dato di tipo persona ne stampi i valori.

Scrivere un programma che chieda all’utente di inserire i dati relativi a delle persone e li salvi in un array del tipo opportuno. Prima di chiedere i dati relativi a una persona il programma chiede all’utente se vuole inserire i dati (l’utente preme Y se vuole inserire una persona). Si supponga che l’utente inserisca al più 100 persone.

## ex. 7
Scrivere un sottoprogramma che tramite il metodo della bisezione trovi la radice di x^3 - x^2 = 0 nell'intervallo dato dato dall'utente

a ogni iterazione calcolero' l'errore come differenza tra errore precedente e xi (significa: quando l'intervallo non cambia tra una iterazione e l'altra piu' di un tot (es 1.e-15))

    err = xi
    xi = a+(b-a)/2.0;
    
    if (f(a)*f(xi)<=0.0) b = xi;
    else if (f(xi)*f(b)<=0.0) a = xi;

    err = xi -err;
